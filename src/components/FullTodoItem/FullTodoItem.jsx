import { Card } from "antd";
import Avatar from "antd/lib/avatar/avatar";
import Meta from "antd/lib/card/Meta";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import { getPhotos, getPosts } from "../../api/api";

function FullTodoItem() {

  const [todoPost, setTodoPost] = useState({});
  const [todoPhoto, setTodoPhoto] = useState({});
  const [isLoading, setisLoading] = useState(false);
  
  const { id } = useParams();

  const showPhotos = async (id) => {
    const photos = await getPhotos(id);
    setTodoPhoto(photos);
  };

  const showPosts = async (id) => {
    const post = await getPosts(id);
    setTodoPost(post);
    setisLoading(true);
  };

  useEffect(() => {
    showPhotos(id);
    showPosts(id);
  }, [id]);

  if (!isLoading) return <p>Loading data</p>;

  return (
    <Card>
      <Meta
        avatar={<Avatar src={todoPhoto.url} />}
        title={todoPost.title}
        description={todoPost.body}
      />
    </Card>
  );
}

export default FullTodoItem;
