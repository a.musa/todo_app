import React, { useEffect, useState } from "react";
import { getTodos } from "../../api/api";
import TodoItem from "../TodoItem/TodoItem";

function Todo() {
  const [isLoadingTodos, setIsLoadingTodos] = useState(false);
  const [todos, setTodos] = useState([]);

  const makeTodoRequest = async () => {
    const todosData = await getTodos();
    setTodos(todosData);
    setIsLoadingTodos(true);
  };

  useEffect(() => {
    makeTodoRequest();
  }, []);

  return (
    <div className="todo">
      {isLoadingTodos &&
        todos.map((item) => {
          return <TodoItem todoItem={item} key={item.id} />;
        })}
    </div>
  );
}

export default Todo;
