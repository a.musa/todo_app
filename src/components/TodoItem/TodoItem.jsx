import { Card } from "antd";
import React from "react";
import { Link } from "react-router-dom";
import "./TodoItem.css";

function TodoItem({ todoItem }) {
  return (
    <Card
      title={`User ${todoItem.id}`}
      extra={<Link to={`/todo/${todoItem.id}`}>Learn more</Link>}
      style={{ width: 300, minHeight: 200 }}
    >
      <p>{todoItem.title}</p>
    </Card>
  );
}

export default TodoItem;
