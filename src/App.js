import Todo from "./components/Todo/Todo";
import "antd/dist/antd.css";
import "./App.css";
import { Route, Switch } from "react-router-dom";
import FullTodoItem from "./components/FullTodoItem/FullTodoItem";

function App() {
  return (
    <div className="App">
      <div className="container">
        <Switch>
          <Route path="/todo/:id" component={() => <FullTodoItem />} />
          <Route path="/" component={() => <Todo />} />
        </Switch>
      </div>
    </div>
  );
}

export default App;
