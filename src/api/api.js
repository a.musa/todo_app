import axios from "axios";

const MAX_ARRAY_LENGTH = 50;

const instance = axios.create({
  baseURL: "https://jsonplaceholder.typicode.com/",
});

export const getTodos = async () => {
  const { data: todos } = await instance.get("todos");
  return todos.slice(0, MAX_ARRAY_LENGTH);
};

export const getPhotos = async (id) => {
  const { data: photos } = await instance.get(`photos/${id}`);
  return photos;
};

export const getPosts = async (id) => {
  const { data: posts } = await instance.get(`posts/${id}`);
  return posts;
};

export default instance;
